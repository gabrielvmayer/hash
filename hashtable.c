#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_SIZE 97

typedef struct node {
    char *chave;
    struct node *next;
} Node;

Node *tabela[MAX_SIZE];

int string_hash(char *s) {
    char c;
    int p = 31, m = 97, hash_value = 0, p_pow = 1;

    while (c = *s++) {
        hash_value = (hash_value + (c - 'a' + 1) * p_pow) % m;
        p_pow = (p_pow * p) % m;
    }
    return (hash_value);
}

void insercao(char *chave) {
    int hash_value = string_hash(chave);
    Node *novo_no = (Node *)malloc(sizeof(Node));
    novo_no->chave = chave;
    novo_no->next = tabela[hash_value];
    tabela[hash_value] = novo_no;

    printf("'%s' inserido na tabela", chave);
}

void remocao(char *chave) {
    int hash_value = string_hash(chave);
    Node *curr = tabela[hash_value];
    Node *prev = NULL;

    while (curr != NULL) {
        if (strcmp(curr->chave, chave) == 0) {
            if (prev == NULL) {
                tabela[hash_value] = curr->next;
            } else {
                prev->next = curr->next;
            }
            free(curr);
            break;
        }
        prev = curr;
        curr = curr->next;
    }

    printf("'%s' removido da tabela", chave);
}

int busca(char *chave) {
    int hash_value = string_hash(chave);
    Node *curr = tabela[hash_value];

    while (curr != NULL) {
        if (strcmp(curr->chave, chave) == 0) {
            return 1;
        }
        curr = curr->next;
    }
    return 0;
}

int main() {
    char s[255];

    fgets(s, 255, stdin);
    s[strcspn(s, "\n")] = '\0';

    printf("%s = %d\n",s, string_hash(s));

    printf("\n");

    // insercao na tabela
    printf("----- INSERINDO '%s' na tabela -----\n", s);
    insercao(s);
    
    // busca na tabela
    printf("\n\n----- BUSCANDO '%s' na tabela -----", s);
    (busca(s)) ?
        printf("\n'%s' encontrado na tabela\n\n", s)
    :
        printf("\n'%s' não encontrado na tabela\n\n", s);

    // remocao da tabela
    printf("----- REMOVENDO '%s' da tabela -----\n", s);
    remocao(s);

    // busca na tabela
    printf("\n\n----- BUSCANDO '%s' na tabela -----", s);
    (busca(s)) ?
        printf("\n'%s' encontrado na tabela\n", s)
    :
        printf("\n'%s' não encontrado na tabela\n", s);

    return 0;
}